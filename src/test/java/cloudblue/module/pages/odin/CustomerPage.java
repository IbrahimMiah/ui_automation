package cloudblue.module.pages.odin;

import cloudblue.utility.Pages;
import cloudblue.utility.browser.Browser;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.ArrayList;
import java.util.List;

public class CustomerPage extends Pages {
    // element for the customer page
    String mainFrame = "mainFrame";
    @FindBy(id = "list_search") private WebElement search_button;
    @FindBy(id = "header_id_0_field_id_0") private WebElement id_search_field;
    @FindBy(xpath = "//table[@id='global_list']/tbody/tr/td[1]") private List<WebElement> id_list;
    By actionButton = By.xpath("../td[last()]/a");
    By statusLabel = By.xpath("../td[7]");

    public CustomerPage(Browser browser) {
        super(browser);
    }

    public CustomerPage load() {
        browser.switchToFrame(mainFrame);
        return this;
    }

    public CustomerDetailsPage openDetailsPageById(String id) {
        for (WebElement id_row : id_list ) {
            if( id_row.getText().trim().equalsIgnoreCase(id)) {
                id_row.click();
                break;
            }
        }
        return new CustomerDetailsPage(browser);
    }

    public CustomerDetailsPage openDetailsPage() {
        id_list.get(0).click();
        wait(5);
        return new CustomerDetailsPage(browser);
    }

    public String readStatusById(String id) {
        String status = "";
        for (WebElement id_row:id_list) {
            if( id_row.getText().equalsIgnoreCase(id)) {
                status = id_row.findElement(statusLabel).getText().trim();
                break;
            }
        }
        return status;
    }
    public void loginAsCustomer() {
        id_list.get(0).findElement(actionButton).click();
        wait(5);
        browser.switchToLastTab();
    }

    public List<String> readCustomerIds() {
        List<String> customerIds = new ArrayList<>();
        for (WebElement id:id_list) {
            customerIds.add(id.getText().trim());
        }
        return customerIds;
    }
    public List<String> readCompanyNames() {
        // return all company name as a String list
        return null;
    }


    public CustomerPage searchWithId(String id) {
        id_search_field.clear();
        id_search_field.sendKeys(id);
        search_button.click();
        return this;
    }
}
