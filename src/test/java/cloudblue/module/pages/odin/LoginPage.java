package cloudblue.module.pages.odin;

import cloudblue.utility.Pages;
import cloudblue.utility.browser.Browser;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends Pages {

    @FindBy(id = "inp_user") private WebElement userNameField;
    @FindBy(id = "inp_password") private WebElement passwordField;
    @FindBy(id = "login") private WebElement loginButton;

    public LoginPage(Browser browser) {
        super(browser);
    }
    public void login(String userName, String password) {
        userNameField.clear();
        userNameField.sendKeys(userName);
        passwordField.clear();
        passwordField.sendKeys(password);
        loginButton.click();
    }
}
