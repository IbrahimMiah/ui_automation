package cloudblue.module.pages.uxone;

import cloudblue.utility.Pages;
import cloudblue.utility.browser.Browser;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CustomerHomePage extends Pages {
    public CustomerHomePage(Browser browser) {
        super(browser);
    }

    @FindBy(id = "ccp-title") private WebElement title;

    public String readTitle() {
        return title.getText().trim();
    }
}
