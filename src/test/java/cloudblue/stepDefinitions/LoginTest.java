package cloudblue.stepDefinitions;

import cloudblue.utility.dataProvider.DataReader;
import cloudblue.utility.dataProvider.RunTimeDataProvider;
import cloudblue.module.pages.Login;
import cloudblue.module.pages.LoginPageFactory;
import cloudblue.reports.ReportManager;
import cloudblue.utility.LogUtil;
import cloudblue.utility.Steps;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class LoginTest extends Steps {
    LogUtil log = new LogUtil();
    private  Login login;
    @Given("The homepage")
    public void the_homepage() throws Exception {
        browser.navigateTo();
    }

    @When("User enters {string} and {string}")
    public void user_enters_and(String string, String string2) throws Throwable {
        login=new Login(browser);
        RunTimeDataProvider.store("customer_id", "CB001");
        System.out.println(RunTimeDataProvider.get("logintest_customer_id"));
        login.enterUsername(string);
        login.enterPassword(string2);
        login.clickLogin();
    }

    @Then("User should be in the homepage")
    public void user_should_be_in_the_homepage() throws Exception {
        // home page
    }

    @Given("^The user enters url \"([^\"]*)\"$")
    public void the_user_enters_url_something(String strArg1) throws Throwable {
        browser.navigateTo(strArg1);
        System.out.println(DataReader.get("order_id"));
//        driver=browser.getDriver();
    }

    @Then("^The word \"([^\"]*)\" should be displayed$")
    public void the_word_something_should_be_displayed(String strArg1) throws Throwable {
        LoginPageFactory loginPageFactory=new LoginPageFactory(browser);
        System.out.println(RunTimeDataProvider.get("logintest_customer_id"));
        loginPageFactory.clickOn();
    }
}
